plugins {
    id("java")
    id("org.jetbrains.kotlin.jvm") version "1.8.22"
    id("org.jetbrains.intellij") version "1.15.0"
}

group = "net.weavemc.intellij"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

val gradleToolingExtension: Configuration by configurations.creating

dependencies {
    // For non-SNAPSHOT versions (unless Jetbrains fixes this...) find the version with:
    // afterEvaluate { println(intellij.ideaDependency.get().buildNumber.substring(intellij.type.get().length + 1)) }
    gradleToolingExtension(libs.groovy)
    gradleToolingExtension(libs.gradleToolingExtension)
    gradleToolingExtension(libs.annotations)

    implementation(libs.bundles.asm)
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
    version.set("2022.3")
    type.set("IC") // Target IDE Platform

    plugins.addAll(
        "java",
        "maven",
        "gradle",
        "Groovy",
        "Kotlin",
//        "org.toml.lang:$pluginTomlVersion",
        "ByteCodeViewer",
        "properties",
    )
}

idea {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

tasks {
    // Set the JVM compatibility versions
    withType<JavaCompile> {
        sourceCompatibility = "17"
        targetCompatibility = "17"
    }
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = "17"
    }

    patchPluginXml {
        sinceBuild.set("222")
        untilBuild.set("232.*")
    }

    signPlugin {
        certificateChain.set(System.getenv("CERTIFICATE_CHAIN"))
        privateKey.set(System.getenv("PRIVATE_KEY"))
        password.set(System.getenv("PRIVATE_KEY_PASSWORD"))
    }

    publishPlugin {
        token.set(System.getenv("PUBLISH_TOKEN"))
    }

    processResources {
        // These templates aren't allowed to be in a directory structure in the output jar
        // But we have a lot of templates that would get real hard to deal with if we didn't have some structure
        // So this just flattens out the fileTemplates/j2ee directory in the jar, while still letting us have directories
        exclude("fileTemplates/j2ee/**")
        from(fileTree("src/main/resources/fileTemplates/j2ee").files) {
            eachFile {
                relativePath = RelativePath(true, "fileTemplates", "j2ee", this.name)
            }
        }
    }
}
