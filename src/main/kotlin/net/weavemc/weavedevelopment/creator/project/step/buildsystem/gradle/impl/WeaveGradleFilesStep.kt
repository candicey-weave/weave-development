package net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle.impl

import com.intellij.ide.starters.local.GeneratorEmptyDirectory
import com.intellij.ide.wizard.NewProjectWizardStep
import com.intellij.openapi.application.WriteAction
import com.intellij.openapi.project.Project
import net.weavemc.weavedevelopment.creator.project.step.AbstractLongRunningAssetsStep
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.BuildSystemPropertiesStep
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle.addGradleWrapperProperties
import net.weavemc.weavedevelopment.creator.project.step.finaliser.impl.JdkProjectSetupFinaliser
import net.weavemc.weavedevelopment.creator.project.step.weaveoption.WeaveOptionStep
import net.weavemc.weavedevelopment.data.GradleDslLanguages.GROOVY
import net.weavemc.weavedevelopment.data.GradleDslLanguages.KOTLIN
import net.weavemc.weavedevelopment.data.JvmLanguages
import net.weavemc.weavedevelopment.data.MinecraftTemplates
import net.weavemc.weavedevelopment.data.WeaveApiVersions
import net.weavemc.weavedevelopment.util.addGradleGitignore
import net.weavemc.weavedevelopment.util.addTemplates
import net.weavemc.weavedevelopment.util.findStep
import net.weavemc.weavedevelopment.util.gitEnabled
import java.io.File

class WeaveGradleFilesStep(parent: NewProjectWizardStep) : AbstractLongRunningAssetsStep(parent) {
    override val description = "Creating Gradle files"

    override fun setupAssets(project: Project) {
        WriteAction.runAndWait<Throwable> { File("${assets.outputDirectory}/.idea").mkdirs() }

        val buildSystemProps = findStep<BuildSystemPropertiesStep<*>>()
        val groupId = buildSystemProps.groupId
        val artifactId = buildSystemProps.artifactId
        val version = buildSystemProps.version
        val mcVersion = data.getUserData(WeaveOptionStep.MC_VERSION_KEY)!!
        val jvmLanguage = data.getUserData(WeaveOptionStep.JVM_LANGUAGE_KEY)!!
        val gradleDslLanguage = data.getUserData(WeaveOptionStep.GRADLE_DSL_LANGUAGE_KEY)!!
        val weaveGradleVersion = data.getUserData(WeaveOptionStep.WEAVE_GRADLE_VERSION_KEY)!!
        val weaveLoaderVersion = data.getUserData(WeaveOptionStep.WEAVE_LOADER_VERSION_KEY)!!
        val weaveApiVersion = data.getUserData(WeaveOptionStep.WEAVE_API_VERSION_KEY)!!
        val useApi = weaveApiVersion != WeaveApiVersions.NONE
        val mappingsType = data.getUserData(WeaveOptionStep.MAPPINGS_Types_KEY)!!
        val javaVersion = findStep<JdkProjectSetupFinaliser>().preferredJdk.ordinal

        fun isJvmLanguageChecker(language: JvmLanguages) = language == jvmLanguage
        val isJvmJava = isJvmLanguageChecker(JvmLanguages.JAVA)
        val isJvmKotlin = isJvmLanguageChecker(JvmLanguages.KOTLIN)

        assets.addTemplateProperties(
            "GROUP_ID" to groupId,
            "ARTIFACT_ID" to artifactId,
            "VERSION" to version,
            "MINECRAFT_VERSION" to mcVersion,
            "IS_JVM_JAVA" to isJvmJava,
            "IS_JVM_KOTLIN" to isJvmKotlin,
            "WEAVE_GRADLE_VERSION" to weaveGradleVersion,
            "WEAVE_LOADER_VERSION" to weaveLoaderVersion,
            "WEAVE_API_VERSION" to weaveApiVersion,
            "USE_API" to useApi,
            "MAPPINGS_TYPE" to mappingsType.toString().lowercase(),
            "JAVA_VERSION" to javaVersion,
        )

        val gradleDsl = when (gradleDslLanguage) {
            GROOVY -> arrayOf(
                "build.gradle" to MinecraftTemplates.WEAVE_BUILD_GRADLE_TEMPLATE_GRVY,
                "settings.gradle" to MinecraftTemplates.WEAVE_SETTINGS_GRADLE_TEMPLATE_GRVY,
            )
            KOTLIN -> arrayOf(
                "build.gradle.kts" to MinecraftTemplates.WEAVE_BUILD_GRADLE_TEMPLATE_KTS,
                "settings.gradle.kts" to MinecraftTemplates.WEAVE_SETTINGS_GRADLE_TEMPLATE_KTS,
            )
        }

        assets.addTemplates(
            project,
            "gradle.properties" to MinecraftTemplates.WEAVE_GRADLE_PROPERTIES_TEMPLATE,
            *gradleDsl,
        )

        val mainClassName = artifactId.filter { it.isLetterOrDigit() || it == '_' || it == '$' }
            .replaceFirstChar { if (it.isDigit()) '_' else it }
            .let {  mainClass ->
                val needsWrappedCharList = listOf('_', '$')
                if (isJvmKotlin && needsWrappedCharList.any { mainClass.contains(it) }) {
                    "`$mainClass`"
                } else {
                    mainClass
                }
            }
        val packageName = groupId
            .filter { it.isLetterOrDigit() || it == '_' || it == '.' || it == '$' }
            .replaceFirstChar { if (it.isDigit()) '_' else it }
        val mainClass = "$packageName.$mainClassName"

        // <name>.mixins.json
        val mixinJson = "${
            artifactId
                .filter { it.isLetterOrDigit() || it == '_' || it == '.' || it == '$' }
                .lowercase()
        }.mixins.json"
        val compatibilityLevel = "JAVA_$javaVersion"
        val mixinPackage = "$groupId.mixin"

        assets.addTemplateProperties(
            "MAIN_CLASS_NAME" to mainClassName,
            "PACKAGE_NAME" to packageName,
            "MAIN_CLASS" to mainClass,
            "MIXIN_JSON" to mixinJson,
            "COMPATIBILITY_LEVEL" to compatibilityLevel,
            "MIXIN_PACKAGE" to mixinPackage,
        )

        val mainClassFile = when (jvmLanguage) {
            JvmLanguages.JAVA -> "src/main/java/${mainClass.replace('.', '/')}.java" to MinecraftTemplates.WEAVE_MAIN_CLASS_JAVA_TEMPLATE
            JvmLanguages.KOTLIN -> "src/main/kotlin/${mainClass.replace('.', '/')}.kt" to MinecraftTemplates.WEAVE_MAIN_CLASS_KOTLIN_TEMPLATE
        }

        assets.addTemplates(
            project,
            mainClassFile,
            "src/main/resources/weave.mod.json" to MinecraftTemplates.WEAVE_MOD_JSON_TEMPLATE,
            "src/main/resources/$mixinJson" to MinecraftTemplates.WEAVE_MIXINS_JSON_TEMPLATE,
        )

        assets.addAssets(
            when (jvmLanguage) {
                JvmLanguages.JAVA -> GeneratorEmptyDirectory("src/main/java/${packageName.replace('.', '/')}")
                JvmLanguages.KOTLIN -> GeneratorEmptyDirectory("src/main/kotlin/${packageName.replace('.', '/')}")
            },
        )

        assets.addGradleWrapperProperties(project)

        if (gitEnabled) {
            assets.addGradleGitignore(project)
        }
    }
}
