package net.weavemc.weavedevelopment.creator.project.step.buildsystem

import com.intellij.ide.wizard.AbstractNewProjectWizardStep
import com.intellij.ide.wizard.NewProjectWizardBaseData
import com.intellij.ide.wizard.NewProjectWizardStep
import com.intellij.openapi.observable.util.bindStorage
import com.intellij.openapi.ui.validation.*
import com.intellij.ui.dsl.builder.*
import net.weavemc.weavedevelopment.bundle.WeaveBundle
import net.weavemc.weavedevelopment.creator.storeToData
import net.weavemc.weavedevelopment.util.SemanticVersion

class BuildSystemPropertiesStep<ParentStep>(private val parent: ParentStep) : AbstractNewProjectWizardStep(parent)
    where ParentStep : NewProjectWizardStep, ParentStep : NewProjectWizardBaseData {

    private val versionValidation = validationErrorIf<String>(WeaveBundle("creator.validation.semantic_version")) {
        SemanticVersion.tryParse(it) == null
    }

    val groupIdProperty = propertyGraph.property("org.example")
        .bindStorage("${javaClass.name}.groupId")
    val artifactIdProperty = propertyGraph.lazyProperty(::suggestArtifactId)
    private val versionProperty = propertyGraph.property("0.1.0")
        .bindStorage("${javaClass.name}.version")

    var groupId by groupIdProperty
    var artifactId by artifactIdProperty
    var version by versionProperty

    init {
        artifactIdProperty.dependsOn(parent.nameProperty, ::suggestArtifactId)
        storeToData()
    }

    private fun suggestArtifactId() = parent.name

    override fun setupUI(builder: Panel) {
        builder.collapsibleGroup(WeaveBundle("creator.ui.group.title")) {
            row(WeaveBundle("creator.ui.group.group_id")) {
                textField()
                    .bindText(groupIdProperty)
                    .columns(COLUMNS_MEDIUM)
                    .validationRequestor(AFTER_GRAPH_PROPAGATION(propertyGraph))
                    .textValidation(CHECK_NON_EMPTY, CHECK_GROUP_ID)
            }
            row(WeaveBundle("creator.ui.group.artifact_id")) {
                textField()
                    .bindText(artifactIdProperty)
                    .columns(COLUMNS_MEDIUM)
                    .validationRequestor(AFTER_GRAPH_PROPAGATION(propertyGraph))
                    .textValidation(CHECK_NON_EMPTY, CHECK_ARTIFACT_ID)
            }
            row(WeaveBundle("creator.ui.group.version")) {
                textField()
                    .bindText(versionProperty)
                    .columns(COLUMNS_MEDIUM)
                    .validationRequestor(AFTER_GRAPH_PROPAGATION(propertyGraph))
                    .textValidation(versionValidation)
            }
        }.expanded = true
    }
}