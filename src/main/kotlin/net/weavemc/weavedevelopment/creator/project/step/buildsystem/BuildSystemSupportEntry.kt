package net.weavemc.weavedevelopment.creator.project.step.buildsystem

import com.intellij.openapi.extensions.RequiredElement
import com.intellij.serviceContainer.BaseKeyedLazyInstance
import com.intellij.util.KeyedLazyInstance
import com.intellij.util.xmlb.annotations.Attribute

class BuildSystemSupportEntry : BaseKeyedLazyInstance<BuildSystemSupport>(), KeyedLazyInstance<BuildSystemSupport> {
    @Attribute("implementation")
    @RequiredElement
    lateinit var implementation: String

    @Attribute("buildSystem")
    @RequiredElement
    lateinit var buildSystem: String

    override fun getKey() = buildSystem

    override fun getImplementationClassName() = implementation
}