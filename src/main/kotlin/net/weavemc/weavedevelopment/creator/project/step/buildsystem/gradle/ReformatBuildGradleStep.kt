package net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle

import com.intellij.ide.wizard.NewProjectWizardStep
import net.weavemc.weavedevelopment.creator.project.step.AbstractReformatFilesStep
import kotlin.io.path.Path

class ReformatBuildGradleStep(parent: NewProjectWizardStep) : AbstractReformatFilesStep(parent) {
    override fun addFilesToReformat() {
        addFileToReformat("build.gradle")
        addFileToReformat("build.gradle.kts")
    }
}