package net.weavemc.weavedevelopment.creator.project

import com.intellij.ide.util.projectWizard.JavaModuleBuilder
import com.intellij.openapi.module.ModuleType
import net.weavemc.weavedevelopment.asset.Assets
import javax.swing.Icon

class WeaveModuleType : com.intellij.openapi.module.JavaModuleType(ID) {
    override fun getName(): String = "Weave Module"

    override fun getDescription(): String = "Module for Weave Development"

    override fun getNodeIcon(isOpened: Boolean): Icon = Assets.MINECRAFT_ICON

    companion object {
        const val NAME = "Weave"
        const val ID = "WEAVE_MODULE_TYPE"
    }
}

object WeaveModuleTypeProvider {
    val instance: ModuleType<JavaModuleBuilder>
        get() = WeaveModuleType()
}