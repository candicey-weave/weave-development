package net.weavemc.weavedevelopment.creator.project.step.weaveoption

import com.intellij.ide.wizard.NewProjectWizardBaseData
import com.intellij.ide.wizard.NewProjectWizardStep
import com.intellij.openapi.observable.util.bindBooleanStorage
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Key
import com.intellij.ui.dsl.builder.*
import com.intellij.util.IncorrectOperationException
import net.weavemc.weavedevelopment.bundle.WeaveBundle
import net.weavemc.weavedevelopment.data.*
import net.weavemc.weavedevelopment.data.GradleDslLanguages.Companion.wrap
import net.weavemc.weavedevelopment.data.JvmLanguages.Companion.wrap
import net.weavemc.weavedevelopment.data.MinecraftVersions.Companion.wrap
import net.weavemc.weavedevelopment.data.WeaveApiVersions.Companion.wrap
import net.weavemc.weavedevelopment.helper.WeaveApiHelper
import net.weavemc.weavedevelopment.helper.WeaveGradleHelper
import net.weavemc.weavedevelopment.helper.WeaveLoaderHelper
import net.weavemc.weavedevelopment.helper.WeaveMappingsHelper
import net.weavemc.weavedevelopment.util.ComparableEnumWrapper

class WeaveOptionStep<ParentStep>(private val parent: ParentStep) : AbstractMcVersionChainStep(parent, WeaveBundle("creator.ui.jvm.jvm_language.label"), WeaveBundle("creator.ui.jvm.gradle_dsl_language.label"), WeaveBundle("creator.ui.weave.weave_gradle_version.label"), WeaveBundle("creator.ui.weave.weave_loader_version.label"), WeaveBundle("creator.ui.weave.api_version.label"), WeaveBundle("creator.ui.weave.mappings.label"), comboBoxBinder = { comboBox, index, label, property ->
    when (index) {
        MINECRAFT_VERSION -> property.set(MinecraftVersions.MC1_8_9.wrap())
        JVM_LANGUAGE -> property.set(JvmLanguages.JAVA.wrap())
        GRADLE_DSL_LANGUAGE -> property.set(GradleDslLanguages.KOTLIN.wrap())
        WEAVE_API_VERSION -> property.set(WeaveApiVersions.values().last().wrap())
    }

    comboBox.bindItem(property)
}),
    NewProjectWizardBaseData by parent
        where ParentStep : NewProjectWizardStep, ParentStep : NewProjectWizardBaseData {
    companion object {
        private const val JVM_LANGUAGE = 1
        private const val GRADLE_DSL_LANGUAGE = 2
        private const val WEAVE_GRADLE_VERSION = 3
        private const val WEAVE_LOADER_VERSION = 4
        private const val WEAVE_API_VERSION = 5
        private const val MAPPINGS = 6

        val MC_VERSION_KEY = Key.create<MinecraftVersions>("${this::class.java}.mcVersion")
        val JVM_LANGUAGE_KEY = Key.create<JvmLanguages>("${this::class.java}.jvmLanguage")
        val GRADLE_DSL_LANGUAGE_KEY = Key.create<GradleDslLanguages>("${this::class.java}.gradleDslLanguage")
        val WEAVE_GRADLE_VERSION_KEY = Key.create<WeaveGradleVersions>("${this::class.java}.weaveGradleVersion")
        val WEAVE_LOADER_VERSION_KEY = Key.create<WeaveLoaderVersions>("${this::class.java}.weaveLoaderVersion")
        val WEAVE_API_VERSION_KEY = Key.create<WeaveApiVersions>("${this::class.java}.weaveApiVersion")
        val MAPPINGS_Types_KEY = Key.create<MappingsTypes>("${this::class.java}.mappings")
    }

    private val useApiProperty = propertyGraph.property(true)
        .bindBooleanStorage("${javaClass.name}.useApi")
    private var useApi by useApiProperty

    @Suppress("UNCHECKED_CAST")
    override fun getAvailableVersions(versionsAbove: List<Comparable<*>>): List<Comparable<*>> {
        val selectedMinecraftVersion by lazy { (versionsAbove[MINECRAFT_VERSION] as ComparableEnumWrapper<MinecraftVersions>.ValueWrapper).value }

        return when (versionsAbove.size) {
            JVM_LANGUAGE -> JvmLanguages.comparable
            GRADLE_DSL_LANGUAGE -> GradleDslLanguages.comparable
            MINECRAFT_VERSION -> MinecraftVersions.comparable
            WEAVE_GRADLE_VERSION -> WeaveGradleHelper.getAvailableVersion(selectedMinecraftVersion)
            WEAVE_LOADER_VERSION -> WeaveLoaderHelper.getAvailableVersion(selectedMinecraftVersion)
            WEAVE_API_VERSION -> WeaveApiHelper.getAvailableVersion(selectedMinecraftVersion)
            MAPPINGS -> WeaveMappingsHelper.getAvailableMappings(selectedMinecraftVersion)
            else -> throw IncorrectOperationException()
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun createComboBox(row: Row, index: Int, items: List<Comparable<*>>): Cell<VersionChainComboBox> {
        return when (index) {
            /*LOADER_VERSION -> {
                val comboBox = super.createComboBox(row, index, items)
                row.checkBox(WeaveBundle("creator.ui.use_api.label"))
                    .bindSelected(useApiProperty)
                row.label("").bindText(
                    getVersionProperty(MINECRAFT_VERSION).transform { mcVersion ->
//                        mcVersion as ComparableEnumWrapper<MinecraftVersions>.ValueWrapper
//                        val matched = apiVersions.versions.any { mcVersion.version in it.gameVersions }
//                        if (matched) {
//                            ""
//                        } else {
//                            "Unable to match API versions to Minecraft version"
//                        }
                        ""
                    },
                ).bindEnabled(useApiProperty).component.foreground = JBColor.YELLOW
                comboBox
            }*/
            /*API_VERSION -> {
                val comboBox = super.createComboBox(row, index, items)
                    .bindEnabled(useApiProperty)
                row.checkBox("Use Weave API").bindSelected(useApiProperty)
                row.label("").bindText(
                    getVersionProperty(MINECRAFT_VERSION).transform { mcVersion ->
                        mcVersion as ComparableEnumWrapper<MinecraftVersions>.ValueWrapper
//                        val matched = apiVersions.versions.any { mcVersion.version in it.gameVersions }
//                        if (matched) {
//                            ""
//                        } else {
//                            "Unable to match API versions to Minecraft version"
//                        }
                        ""
                    },
                ).bindEnabled(useApiProperty).component.foreground = JBColor.YELLOW
                comboBox
            }*/

            else -> super.createComboBox(row, index, items)
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun setupProject(project: Project) {
        super.setupProject(project)
        data.putUserData(MC_VERSION_KEY, (getVersion(MINECRAFT_VERSION) as ComparableEnumWrapper<MinecraftVersions>.ValueWrapper).value)
        data.putUserData(JVM_LANGUAGE_KEY, (getVersion(JVM_LANGUAGE) as ComparableEnumWrapper<JvmLanguages>.ValueWrapper).value)
        data.putUserData(GRADLE_DSL_LANGUAGE_KEY, (getVersion(GRADLE_DSL_LANGUAGE) as ComparableEnumWrapper<GradleDslLanguages>.ValueWrapper).value)
        data.putUserData(WEAVE_GRADLE_VERSION_KEY, (getVersion(WEAVE_GRADLE_VERSION) as ComparableEnumWrapper<WeaveGradleVersions>.ValueWrapper).value)
        data.putUserData(WEAVE_LOADER_VERSION_KEY, (getVersion(WEAVE_LOADER_VERSION) as ComparableEnumWrapper<WeaveLoaderVersions>.ValueWrapper).value)
        data.putUserData(WEAVE_API_VERSION_KEY, (getVersion(WEAVE_API_VERSION) as ComparableEnumWrapper<WeaveApiVersions>.ValueWrapper).value)
        data.putUserData(MAPPINGS_Types_KEY, getVersion(MAPPINGS) as MappingsTypes)
    }
}