/*
 * Minecraft Development for IntelliJ
 *
 * https://mcdev.io/
 *
 * Copyright (C) 2023 minecraft-dev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, version 3.0 only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle

import com.intellij.ide.wizard.NewProjectWizardStep
import com.intellij.openapi.project.DumbService
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Key
import net.weavemc.weavedevelopment.creator.project.step.AbstractLongRunningStep
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.FixedAssetsNewProjectWizardStep
import net.weavemc.weavedevelopment.data.MinecraftTemplates
import net.weavemc.weavedevelopment.util.SemanticVersion
import net.weavemc.weavedevelopment.util.addTemplates
import net.weavemc.weavedevelopment.util.runGradleTask
import java.nio.file.Path

val DEFAULT_GRADLE_VERSION = SemanticVersion.release(8, 1, 1)
val GRADLE_VERSION_KEY = Key.create<SemanticVersion>("mcdev.gradleVersion")

fun FixedAssetsNewProjectWizardStep.addGradleWrapperProperties(project: Project) {
    val gradleVersion = data.getUserData(GRADLE_VERSION_KEY) ?: DEFAULT_GRADLE_VERSION
    addTemplateProperties("GRADLE_WRAPPER_VERSION" to gradleVersion)
    addTemplates(project, "gradle/wrapper/gradle-wrapper.properties" to MinecraftTemplates.GRADLE_WRAPPER_PROPERTIES)
}

abstract class AbstractRunGradleTaskStep(parent: NewProjectWizardStep) : AbstractLongRunningStep(parent) {
    abstract val task: String
    override val description get() = "Running Gradle task: '$task'"

    override fun perform(project: Project) {
        val outputDirectory = context.projectFileDirectory
        DumbService.getInstance(project).runWhenSmart {
            runGradleTask(project, Path.of(outputDirectory)) { settings ->
                settings.taskNames = listOf(task)
            }
        }
    }
}

