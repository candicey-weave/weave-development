package net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle.impl

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.*
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle.*
import net.weavemc.weavedevelopment.util.mapFirstNotNull
import org.jetbrains.plugins.groovy.lang.psi.GroovyFile
import org.jetbrains.plugins.groovy.lang.psi.GroovyPsiElementFactory
import org.jetbrains.plugins.groovy.lang.psi.api.statements.blocks.GrClosableBlock
import org.jetbrains.plugins.groovy.lang.psi.api.statements.expressions.path.GrMethodCallExpression
import org.jetbrains.plugins.groovy.lang.psi.api.util.GrStatementOwner

class GroovyGradleFile(override val psi: GroovyFile) : GradleFile {
    override fun addRepositories(project: Project, repositories: List<BuildRepository>) {
        val reposBlock = findOrCreateGroovyBlock(project, psi, "repositories")
        val elementFactory = GroovyPsiElementFactory.getInstance(project)
        for (repo in repositories) {
            if (BuildSystemType.GRADLE !in repo.buildSystems) {
                continue
            }
            val mavenBlock =
                elementFactory.createStatementFromText("maven {\n}", reposBlock) as GrMethodCallExpression
            val mavenClosure = mavenBlock.closureArguments[0]
            if (repo.id.isNotBlank()) {
                val idStatement =
                    elementFactory.createStatementFromText("name = ${makeStringLiteral(repo.id)}")
                mavenClosure.addStatementBefore(idStatement, null)
            }
            val urlStatement =
                elementFactory.createStatementFromText("url = ${makeStringLiteral(repo.url)}")
            mavenClosure.addStatementBefore(urlStatement, null)
            reposBlock.addStatementBefore(mavenBlock, null)
        }
    }

    override fun addDependencies(project: Project, dependencies: List<BuildDependency>) {
        val depsBlock = findOrCreateGroovyBlock(project, psi, "dependencies")
        val elementFactory = GroovyPsiElementFactory.getInstance(project)
        for (dep in dependencies) {
            val gradleConfig = dep.gradleConfiguration ?: continue
            val stmt = elementFactory.createStatementFromText(
                "$gradleConfig \"${escapeGString(dep.groupId)}:${
                    escapeGString(dep.artifactId)
                }:${escapeGString(dep.version)}\"",
                depsBlock,
            )
            depsBlock.addStatementBefore(stmt, null)
        }
    }

    override fun addPlugins(project: Project, plugins: List<GradlePlugin>) {
        val pluginsBlock = findOrCreateGroovyBlock(project, psi, "plugins", first = true)
        val elementFactory = GroovyPsiElementFactory.getInstance(project)
        for (plugin in plugins) {
            val stmt = elementFactory.createStatementFromText(makePluginStatement(plugin, false))
            pluginsBlock.addStatementBefore(stmt, null)
        }
    }

    private fun findGroovyBlock(element: GrStatementOwner, name: String): GrClosableBlock? {
        return element.statements
            .mapFirstNotNull { call ->
                if (call is GrMethodCallExpression && call.callReference?.methodName == name) {
                    call.closureArguments.firstOrNull()
                } else {
                    null
                }
            }
    }

    private fun findOrCreateGroovyBlock(
        project: Project,
        element: GrStatementOwner,
        name: String,
        first: Boolean = false,
    ): GrClosableBlock {
        findGroovyBlock(element, name)?.let { return it }
        val block = GroovyPsiElementFactory.getInstance(project).createStatementFromText("$name {\n}", element)
        val anchor = if (first) {
            element.statements.firstOrNull()
        } else {
            null
        }
        return (element.addStatementBefore(block, anchor) as GrMethodCallExpression).closureArguments.first()
    }

    class Type : GradleFile.Type {
        override fun createGradleFile(psiFile: PsiFile) = (psiFile as? GroovyFile)?.let(::GroovyGradleFile)
    }
}