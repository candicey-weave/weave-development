package net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle

data class GradlePlugin(
    val id: String,
    val version: String? = null,
    val apply: Boolean = true,
)