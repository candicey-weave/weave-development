/*
 * Minecraft Development for IntelliJ
 *
 * https://mcdev.io/
 *
 * Copyright (C) 2023 minecraft-dev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, version 3.0 only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.weavemc.weavedevelopment.creator.project.step.finaliser

import com.intellij.ide.wizard.AbstractNewProjectWizardStep
import com.intellij.ide.wizard.NewProjectWizardStep
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.openapi.ui.validation.AFTER_GRAPH_PROPAGATION
import com.intellij.openapi.ui.validation.DialogValidation
import com.intellij.ui.dsl.builder.Panel
import net.weavemc.weavedevelopment.creator.project.step.NewProjectWizardChainStep.Companion.nextStep
import net.weavemc.weavedevelopment.util.mapFirstNotNull
import javax.swing.JPanel

class ProjectSetupFinaliserWizardStep(parent: NewProjectWizardStep) : AbstractNewProjectWizardStep(parent) {
    private val finalizers: List<ProjectSetupFinaliser> by lazy {
        val factories = ProjectSetupFinaliser.EP_NAME.extensionList
        val result = mutableListOf<ProjectSetupFinaliser>()
        if (factories.isNotEmpty()) {
            var par: NewProjectWizardStep = this
            for (factory in factories) {
                val finalizer = factory.create(par)
                result += finalizer
                par = finalizer
            }
        }
        result
    }
    private val step by lazy {
        when (finalizers.size) {
            0 -> null
            1 -> finalizers[0]
            else -> {
                var step = finalizers[0].nextStep { finalizers[1] }
                for (i in 2 until finalizers.size) {
                    step = step.nextStep { finalizers[i] }
                }
                step
            }
        }
    }

    override fun setupUI(builder: Panel) {
        step?.setupUI(builder)
        if (finalizers.isNotEmpty()) {
            builder.row {
                cell(JPanel())
                    .validationRequestor(AFTER_GRAPH_PROPAGATION(propertyGraph))
                    .validation(
                        DialogValidation {
                            finalizers.mapFirstNotNull(ProjectSetupFinaliser::validate)?.let(::ValidationInfo)
                        }
                    )
            }
        }
    }

    override fun setupProject(project: Project) {
        step?.setupProject(project)
    }
}

