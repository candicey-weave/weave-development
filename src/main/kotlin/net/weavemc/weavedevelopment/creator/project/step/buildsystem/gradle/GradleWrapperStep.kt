package net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle

import com.intellij.ide.wizard.NewProjectWizardStep

class GradleWrapperStep(parent: NewProjectWizardStep) : AbstractRunGradleTaskStep(parent) {
    override val task = "wrapper"
}