package net.weavemc.weavedevelopment.creator.project.step.finaliser

import com.intellij.ide.wizard.NewProjectWizardStep
import com.intellij.openapi.extensions.ExtensionPointName

/**
 * A step applied after all other steps for all Minecraft project creators. These steps can also block project creation
 * by providing extra validations.
 *
 * To add custom project setup finalizers, register a [Factory] to the
 * `com.demonwav.minecraft-dev.projectSetupFinalizer` extension point.
 */
interface ProjectSetupFinaliser : NewProjectWizardStep {
    companion object {
        val EP_NAME = ExtensionPointName<Factory>("net.weavemc.weavedevelopment.projectSetupFinaliser")
    }

    /**
     * Validates the existing settings of this wizard.
     *
     * @return `null` if the settings are valid, or an error message if they are invalid.
     */
    fun validate(): String? = null

    interface Factory {
        fun create(parent: NewProjectWizardStep): ProjectSetupFinaliser
    }
}