package net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle

import com.intellij.execution.RunManager
import com.intellij.ide.ui.UISettings
import com.intellij.ide.wizard.NewProjectWizardStep
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.StartupManager
import com.intellij.openapi.wm.WindowManager
import com.intellij.openapi.wm.ex.StatusBarEx
import net.weavemc.weavedevelopment.bundle.WeaveBundle
import net.weavemc.weavedevelopment.creator.project.step.AbstractLongRunningStep
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.BuildSystemPropertiesStep
import net.weavemc.weavedevelopment.util.findStep
import net.weavemc.weavedevelopment.util.invokeLater
import net.weavemc.weavedevelopment.util.notifyCreatedProjectNotOpened
import net.weavemc.weavedevelopment.util.virtualFileOrError
import org.jetbrains.plugins.gradle.service.execution.GradleExternalTaskConfigurationType
import org.jetbrains.plugins.gradle.service.execution.GradleRunConfiguration
import org.jetbrains.plugins.gradle.service.project.open.canLinkAndRefreshGradleProject
import org.jetbrains.plugins.gradle.service.project.open.linkAndRefreshGradleProject
import java.nio.file.Path
import java.util.concurrent.CountDownLatch

open class GradleImportStep(parent: NewProjectWizardStep) : AbstractLongRunningStep(parent) {
    override val description
        get() = WeaveBundle("creator.step.gradle.import_gradle.description")

    open val additionalRunTasks = emptyList<String>()

    override fun perform(project: Project) {
        if (!project.isInitialized) {
            notifyCreatedProjectNotOpened()
            return
        }

        val rootDirectory = Path.of(context.projectFileDirectory)
        val buildSystemProps = findStep<BuildSystemPropertiesStep<*>>()

        // Tell IntelliJ to import this project
        rootDirectory.virtualFileOrError.refresh(false, true)

        val latch = CountDownLatch(1)

        invokeLater(project.disposed) {
            val path = rootDirectory.toAbsolutePath().toString()
            if (canLinkAndRefreshGradleProject(path, project, false)) {
                linkAndRefreshGradleProject(path, project)
                showProgress(project)
            }

            StartupManager.getInstance(project).runAfterOpened {
                latch.countDown()
            }
        }

        // Set up the run config
        // Get the gradle external task type, this is what sets it as a gradle task
        addRunTaskConfiguration(project, rootDirectory, buildSystemProps, "build")
        for (tasks in additionalRunTasks) {
            addRunTaskConfiguration(project, rootDirectory, buildSystemProps, tasks)
        }

        if (!ApplicationManager.getApplication().isDispatchThread) {
            latch.await()
        }
    }

    private fun addRunTaskConfiguration(
        project: Project,
        rootDirectory: Path,
        buildSystemProps: BuildSystemPropertiesStep<*>,
        task: String,
    ) {
        val gradleType = GradleExternalTaskConfigurationType.getInstance()

        val runManager = RunManager.getInstance(project)
        val runConfigName = buildSystemProps.artifactId + ' ' + task

        val runConfiguration = GradleRunConfiguration(project, gradleType.factory, runConfigName)

        // Set relevant gradle values
        runConfiguration.settings.externalProjectPath = rootDirectory.toAbsolutePath().toString()
        runConfiguration.settings.executionName = runConfigName
        runConfiguration.settings.taskNames = listOf(task)

        runConfiguration.isAllowRunningInParallel = false

        val settings = runManager.createConfiguration(
            runConfiguration,
            gradleType.factory,
        )

        settings.isActivateToolWindowBeforeRun = true
        settings.storeInLocalWorkspace()

        runManager.addConfiguration(settings)
        if (runManager.selectedConfiguration == null) {
            runManager.selectedConfiguration = settings
        }
    }

    // Show the background processes window for setup tasks
    private fun showProgress(project: Project) {
        if (!UISettings.getInstance().showStatusBar || UISettings.getInstance().presentationMode) {
            return
        }

        val statusBar = WindowManager.getInstance().getStatusBar(project) as? StatusBarEx ?: return
        statusBar.isProcessWindowOpen = true
    }
}