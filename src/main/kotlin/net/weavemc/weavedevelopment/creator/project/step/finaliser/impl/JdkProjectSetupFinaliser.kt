package net.weavemc.weavedevelopment.creator.project.step.finaliser.impl

import com.intellij.ide.wizard.AbstractNewProjectWizardStep
import com.intellij.ide.wizard.NewProjectWizardStep
import com.intellij.openapi.observable.properties.GraphProperty
import com.intellij.openapi.projectRoots.JavaSdk
import com.intellij.openapi.projectRoots.JavaSdkVersion
import com.intellij.openapi.projectRoots.Sdk
import com.intellij.ui.JBColor
import com.intellij.ui.dsl.builder.Panel
import com.intellij.ui.dsl.builder.Placeholder
import net.weavemc.weavedevelopment.bundle.WeaveBundle
import net.weavemc.weavedevelopment.creator.project.step.finaliser.ProjectSetupFinaliser
import net.weavemc.weavedevelopment.creator.storeToData
import javax.swing.JLabel

class JdkProjectSetupFinaliser(
    parent: NewProjectWizardStep,
) : AbstractNewProjectWizardStep(parent), ProjectSetupFinaliser {
    private val sdkProperty: GraphProperty<Sdk?> = propertyGraph.property(null)
    private var sdk by sdkProperty
    private var sdkComboBox: JdkComboBoxWithPreference? = null
    private var preferredJdkLabel: Placeholder? = null
    private var preferredJdkReason = WeaveBundle("creator.validation.jdk_preferred_default_reason")

    var preferredJdk: JavaSdkVersion = JavaSdkVersion.JDK_17
        private set

    fun setPreferredJdk(value: JavaSdkVersion, reason: String) {
        preferredJdk = value
        preferredJdkReason = reason
        sdkComboBox?.setPreferredJdk(value)
        updatePreferredJdkLabel()
    }

    init {
        storeToData()

        sdkProperty.afterChange {
            updatePreferredJdkLabel()
        }
    }

    private fun updatePreferredJdkLabel() {
        val sdk = this.sdk ?: return
        val version = JavaSdk.getInstance().getVersion(sdk) ?: return
        if (version == preferredJdk) {
            preferredJdkLabel?.component = null
        } else {
            preferredJdkLabel?.component =
                JLabel(WeaveBundle("creator.validation.jdk_preferred", preferredJdk.description, preferredJdkReason))
                    .also { it.foreground = JBColor.YELLOW }
        }
    }

    override fun setupUI(builder: Panel) {
        with(builder) {
            row("JDK:") {
                val sdkComboBox = jdkComboBoxWithPreference(context, sdkProperty, "${javaClass.name}.sdk")
                this@JdkProjectSetupFinaliser.sdkComboBox = sdkComboBox.component
                this@JdkProjectSetupFinaliser.preferredJdkLabel = placeholder()
                updatePreferredJdkLabel()
            }
        }
    }

    class Factory : ProjectSetupFinaliser.Factory {
        override fun create(parent: NewProjectWizardStep) = JdkProjectSetupFinaliser(parent)
    }
}