package net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle.impl

import com.intellij.ide.wizard.NewProjectWizardStep
import net.weavemc.weavedevelopment.creator.project.step.NewProjectWizardChainStep.Companion.nextStep
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.BuildSystemSupport
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle.GradleImportStep
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle.GradleWrapperStep
import net.weavemc.weavedevelopment.util.EmptyStep

class WeaveGradleSupport : BuildSystemSupport {
    override val preferred = true

    override fun createStep(step: String, parent: NewProjectWizardStep): NewProjectWizardStep {
        return when (step) {
            BuildSystemSupport.PRE_STEP -> WeaveGradleFilesStep(parent).nextStep(::GradleWrapperStep)
            BuildSystemSupport.POST_STEP -> GradleImportStep(parent)
            else -> EmptyStep(parent)
        }
    }
}