/*
 * Minecraft Development for IntelliJ
 *
 * https://mcdev.io/
 *
 * Copyright (C) 2023 minecraft-dev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, version 3.0 only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle

import com.intellij.openapi.extensions.ExtensionPointName
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.text.StringUtil
import com.intellij.psi.PsiFile
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.BuildDependency
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.BuildRepository
import org.jetbrains.annotations.ApiStatus.Internal

@Internal
interface GradleFile {
    companion object {
        val EP_NAME = ExtensionPointName<Type>("net.weavemc.weavedevelopment.gradleFileType")
    }

    val psi: PsiFile

    fun addRepositories(project: Project, repositories: List<BuildRepository>)
    fun addDependencies(project: Project, dependencies: List<BuildDependency>)
    fun addPlugins(project: Project, plugins: List<GradlePlugin>)

    interface Type {
        fun createGradleFile(psiFile: PsiFile): GradleFile?
    }
}

fun makeStringLiteral(str: String): String {
    return "\"${escapeGString(str)}\""
}

fun escapeGString(str: String): String {
    return StringUtil.escapeStringCharacters(str.length, str, "\"\$", StringBuilder()).toString()
}

fun makePluginStatement(plugin: GradlePlugin, kotlin: Boolean): String {
    return buildString {
        if (kotlin) {
            append("id(${makeStringLiteral(plugin.id)})")
        } else {
            append("id ${makeStringLiteral(plugin.id)}")
        }
        plugin.version?.let { append(" version ${makeStringLiteral(it)}") }
        if (!plugin.apply) {
            append(" apply false")
        }
    }
}
