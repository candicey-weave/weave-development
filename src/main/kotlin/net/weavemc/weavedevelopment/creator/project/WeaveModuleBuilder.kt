package net.weavemc.weavedevelopment.creator.project

import com.intellij.ide.projectWizard.ProjectSettingsStep
import com.intellij.ide.util.projectWizard.WizardContext
import com.intellij.ide.wizard.*
import com.intellij.openapi.roots.ModifiableRootModel
import net.weavemc.weavedevelopment.asset.Assets
import net.weavemc.weavedevelopment.creator.project.step.NewProjectWizardChainStep.Companion.nextStep
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.BuildSystemPropertiesStep
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle.impl.WeaveGradleFilesStep
import net.weavemc.weavedevelopment.creator.project.step.finaliser.ProjectSetupFinaliserWizardStep
import net.weavemc.weavedevelopment.creator.project.step.weaveoption.WeaveOptionStep
import javax.swing.Icon

class WeaveModuleBuilder : AbstractNewProjectWizardBuilder() {
    override fun getPresentableName(): String = WeaveModuleType.NAME

    override fun getGroupName(): String = WeaveModuleType.NAME

    override fun getParentGroup(): String = WeaveModuleType.NAME

    override fun getBuilderId(): String = ID

    override fun getDescription(): String = "Module for Weave Development"

    override fun getNodeIcon(): Icon = Assets.MINECRAFT_ICON

    override fun setupRootModel(modifiableRootModel: ModifiableRootModel) {
        if (moduleJdk != null) {
            modifiableRootModel.sdk = moduleJdk
        } else {
            modifiableRootModel.inheritSdk()
        }
    }

    override fun createStep(context: WizardContext): NewProjectWizardStep =
        RootNewProjectWizardStep(context)
            .nextStep(::NewProjectWizardBaseStep)
            .nextStep(::GitNewProjectWizardStep)
            .nextStep(::WeaveOptionStep)
            .nextStep(::BuildSystemPropertiesStep)
            .nextStep(::WeaveGradleFilesStep)
            .nextStep(::ProjectSetupFinaliserWizardStep)

    override fun getIgnoredSteps() = listOf(ProjectSettingsStep::class.java)

    companion object {
        const val ID = "WEAVE_MODULE_BUILDER"
    }
}