package net.weavemc.weavedevelopment.creator.project.step.buildsystem.gradle

import com.intellij.ide.wizard.NewProjectWizardStep
import com.intellij.lang.properties.psi.PropertiesFile
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.fileEditor.impl.NonProjectFileWritingAccessProvider
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VfsUtil
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiManager
import net.weavemc.weavedevelopment.bundle.WeaveBundle
import net.weavemc.weavedevelopment.creator.project.step.AbstractLongRunningStep
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.BuildDependency
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.BuildRepository
import net.weavemc.weavedevelopment.util.*
import java.nio.file.Path

abstract class AbstractPatchGradleFilesStep(parent: NewProjectWizardStep) : AbstractLongRunningStep(parent) {
    override val description
        get() = WeaveBundle("creator.step.gradle.patch_gradle.description")

    abstract fun patch(project: Project, gradleFiles: GradleFiles)

    protected fun addRepositories(project: Project, buildGradle: GradleFile?, repositories: List<BuildRepository>) {
        if (buildGradle == null || repositories.isEmpty()) {
            return
        }

        buildGradle.psi.runWriteAction {
            buildGradle.addRepositories(project, repositories)
        }
    }

    protected fun addDependencies(project: Project, buildGradle: GradleFile?, dependencies: List<BuildDependency>) {
        if (buildGradle == null || dependencies.isEmpty()) {
            return
        }

        buildGradle.psi.runWriteAction {
            buildGradle.addDependencies(project, dependencies)
        }
    }

    protected fun addPlugins(project: Project, buildGradle: GradleFile?, plugins: List<GradlePlugin>) {
        if (buildGradle == null || plugins.isEmpty()) {
            return
        }

        buildGradle.psi.runWriteAction {
            buildGradle.addPlugins(project, plugins)
        }
    }

    override fun perform(project: Project) {
        invokeAndWait {
            if (project.isDisposed || !project.isInitialized) {
                notifyCreatedProjectNotOpened()
                return@invokeAndWait
            }

            runWriteTask {
                val rootDir = VfsUtil.findFile(Path.of(context.projectFileDirectory), true)
                    ?: return@runWriteTask
                val gradleFiles = GradleFiles(project, rootDir)
                NonProjectFileWritingAccessProvider.disableChecksDuring {
                    patch(project, gradleFiles)
                    gradleFiles.commit()
                }
            }
        }
    }

    class GradleFiles(
        private val project: Project,
        private val rootDir: VirtualFile,
    ) {
        private val lazyBuildGradle = lazy {
            val file = rootDir.findChild("build.gradle") ?: rootDir.findChild("build.gradle.kts")
                ?: return@lazy null
            makeGradleFile(file)
        }
        private val lazySettingsGradle = lazy {
            val file = rootDir.findChild("settings.gradle") ?: rootDir.findChild("settings.gradle.kts")
                ?: return@lazy null
            makeGradleFile(file)
        }
        private val lazyGradleProperties = lazy {
            val file = rootDir.findChild("gradle.properties") ?: return@lazy null
            PsiManager.getInstance(project).findFile(file) as? PropertiesFile
        }

        val buildGradle by lazyBuildGradle
        val settingsGradle by lazySettingsGradle
        val gradleProperties by lazyGradleProperties

        private fun makeGradleFile(virtualFile: VirtualFile): GradleFile? {
            val psi = PsiManager.getInstance(project).findFile(virtualFile) ?: return null
            return GradleFile.EP_NAME.extensions.mapFirstNotNull { it.createGradleFile(psi) }
        }

        fun commit() {
            val files = mutableListOf<PsiFile>()
            if (lazyBuildGradle.isInitialized()) {
                buildGradle?.psi?.let { files += it }
            }
            if (lazySettingsGradle.isInitialized()) {
                settingsGradle?.psi?.let { files += it }
            }
            if (lazyGradleProperties.isInitialized()) {
                (gradleProperties as? PsiFile)?.let { files += it }
            }

            val psiDocumentManager = PsiDocumentManager.getInstance(project)
            val fileDocumentManager = FileDocumentManager.getInstance()
            for (file in files) {
                val document = psiDocumentManager.getDocument(file) ?: continue
                fileDocumentManager.saveDocument(document)
            }
        }
    }
}