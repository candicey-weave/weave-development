package net.weavemc.weavedevelopment.creator

import com.intellij.ide.wizard.NewProjectWizardStep
import com.intellij.openapi.util.Key

private val stepClassToKey = mutableMapOf<Class<*>, Key<*>>()

@Suppress("UNCHECKED_CAST")
@PublishedApi
internal fun <T : NewProjectWizardStep> getOrCreateClassKey(clazz: Class<out T>) =
    stepClassToKey.computeIfAbsent(clazz) {
        Key.create<T>(it.name)
    } as Key<T>

private val stepClassToWhenAvailableKey = mutableMapOf<Class<*>, Key<*>>()

@Suppress("UNCHECKED_CAST")
@PublishedApi
internal fun <T : NewProjectWizardStep> getWhenAvailableKey(clazz: Class<out T>) =
    stepClassToWhenAvailableKey[clazz] as Key<MutableList<(T) -> Unit>>?

inline fun <reified T : NewProjectWizardStep> T.storeToData() {
    storeToData(T::class.java)
}

fun <T : NewProjectWizardStep> T.storeToData(clazz: Class<out T>) {
    data.putUserData(getOrCreateClassKey(clazz), this)
    getWhenAvailableKey(clazz)?.let { whenAvailableKey ->
        data.getUserData(whenAvailableKey)?.let { whenAvailable ->
            for (func in whenAvailable) {
                func(this)
            }
            data.putUserData(whenAvailableKey, null)
        }
    }
}
