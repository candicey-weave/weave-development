package net.weavemc.weavedevelopment.helper

import net.weavemc.weavedevelopment.data.MinecraftVersions
import net.weavemc.weavedevelopment.data.WeaveLoaderVersions
import net.weavemc.weavedevelopment.util.ComparableEnumWrapper

object WeaveLoaderHelper {
    fun getAvailableVersion(minecraftVersions: MinecraftVersions): List<ComparableEnumWrapper<WeaveLoaderVersions>.ValueWrapper> {
        return WeaveLoaderVersions.comparable
    }
}