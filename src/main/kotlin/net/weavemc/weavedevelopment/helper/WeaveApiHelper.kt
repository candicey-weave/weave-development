package net.weavemc.weavedevelopment.helper

import net.weavemc.weavedevelopment.data.MinecraftVersions
import net.weavemc.weavedevelopment.data.MinecraftVersions.*
import net.weavemc.weavedevelopment.data.WeaveApiVersions
import net.weavemc.weavedevelopment.data.WeaveApiVersions.Companion.wrap
import net.weavemc.weavedevelopment.util.ComparableEnumWrapper

object WeaveApiHelper {
    val supportedVersion: List<MinecraftVersions> =
        listOf(
            MC1_7_10,
            MC1_8_9,
            MC1_12_2,
            MC1_16_5,
            MC1_20_2,
        )

    fun getAvailableVersion(minecraftVersions: MinecraftVersions): List<ComparableEnumWrapper<WeaveApiVersions>.ValueWrapper> {
        return if (minecraftVersions in supportedVersion) {
            WeaveApiVersions.values().map(WeaveApiVersions::wrap)
        } else {
            listOf(WeaveApiVersions.NONE.wrap())
        }
    }
}