package net.weavemc.weavedevelopment.helper

import net.weavemc.weavedevelopment.data.MappingsTypes
import net.weavemc.weavedevelopment.data.MappingsTypes.*
import net.weavemc.weavedevelopment.data.MinecraftVersions
import net.weavemc.weavedevelopment.data.MinecraftVersions.MC1_11_2
import net.weavemc.weavedevelopment.data.MinecraftVersions.MC1_12_2

object WeaveMappingsHelper {
    fun getAvailableMappings(minecraftVersion: MinecraftVersions): List<MappingsTypes> {
        val semantic = minecraftVersion.semantic

        val availableMappings = mutableListOf<MappingsTypes>()

        infix fun MappingsTypes.condition(condition: Boolean) {
            if (condition) {
                availableMappings.add(this)
            }
        }

        SEARGE condition (semantic <= MC1_12_2.semantic)
        MOJMAP condition (semantic > MC1_12_2.semantic)
        YARN   condition (semantic >= MC1_11_2.semantic)

        return availableMappings
    }
}