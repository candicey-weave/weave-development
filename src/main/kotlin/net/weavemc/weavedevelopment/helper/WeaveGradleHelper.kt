package net.weavemc.weavedevelopment.helper

import net.weavemc.weavedevelopment.data.MinecraftVersions
import net.weavemc.weavedevelopment.data.WeaveGradleVersions
import net.weavemc.weavedevelopment.data.WeaveLoaderVersions
import net.weavemc.weavedevelopment.util.ComparableEnumWrapper

object WeaveGradleHelper {
    fun getAvailableVersion(minecraftVersions: MinecraftVersions): List<ComparableEnumWrapper<WeaveGradleVersions>.ValueWrapper> {
        return WeaveGradleVersions.comparable
    }
}