package net.weavemc.weavedevelopment.data

import net.weavemc.weavedevelopment.util.ComparableEnumWrapper

enum class GradleDslLanguages {
    GROOVY,
    KOTLIN;

    override fun toString(): String {
        return name[0] + name.substring(1).lowercase()
    }

    companion object {
        val comparable by ComparableEnumWrapper(values()) { current, other -> -current.value.compareTo(other.value) }

        fun wrap(version: GradleDslLanguages) = comparable.find { it.value == version } ?: error("Unknown Gradle DSL language $version")

        @JvmName("wrapExtension")
        fun GradleDslLanguages.wrap() = wrap(this)
    }
}