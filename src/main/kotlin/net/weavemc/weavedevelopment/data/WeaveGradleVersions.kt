package net.weavemc.weavedevelopment.data

import net.weavemc.weavedevelopment.util.ComparableEnumWrapper
import net.weavemc.weavedevelopment.util.SemanticVersion

enum class WeaveGradleVersions(
    versionString: String? = null,
    delimiter: String = ".",
) {
    V0_1_0;

    val semantic by lazy {
        val version = versionString?.split(delimiter) ?: name.substring(1).split("_")
        SemanticVersion.release(*version.map(String::toInt).toIntArray())
    }

    override fun toString(): String = semantic.toString()

    companion object {
        val comparable by ComparableEnumWrapper.withSemantic(values(), WeaveGradleVersions::semantic)
    }
}