package net.weavemc.weavedevelopment.data

import net.weavemc.weavedevelopment.util.ComparableEnumWrapper

enum class JvmLanguages {
    JAVA,
    KOTLIN;

    override fun toString(): String {
        return name[0] + name.substring(1).lowercase()
    }

    companion object {
        val comparable by ComparableEnumWrapper(values()) { current, other -> -current.value.compareTo(other.value) }

        fun wrap(version: JvmLanguages) = comparable.find { it.value == version } ?: error("Unknown JVM language $version")

        @JvmName("wrapExtension")
        fun JvmLanguages.wrap() = wrap(this)
    }
}
