/*
 * Minecraft Development for IntelliJ
 *
 * https://mcdev.io/
 *
 * Copyright (C) 2023 minecraft-dev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, version 3.0 only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.weavemc.weavedevelopment.data

import com.intellij.openapi.projectRoots.JavaSdkVersion
import net.weavemc.weavedevelopment.util.ComparableEnumWrapper
import net.weavemc.weavedevelopment.util.SemanticVersion

enum class MinecraftVersions(
    versionString: String? = null,
    delimiter: String = ".",
) {
    MC1_7_10,
    MC1_8_9,
    MC1_9_4,
    MC1_10_2,
    MC1_11_2,
    MC1_12_2,
    MC1_13_2,
    MC1_14_4,
    MC1_15_2,
    MC1_16_5,
    MC1_17_1,
    MC1_18_2,
    MC1_19_4,
    MC1_20_2;

    val semantic by lazy {
        val version = versionString?.split(delimiter) ?: name.substring(2).split("_")
        SemanticVersion.release(*version.map(String::toInt).toIntArray())
    }

    override fun toString(): String = semantic.toString()

    companion object {
        val comparable by ComparableEnumWrapper.withSemantic(values(), MinecraftVersions::semantic, ComparableEnumWrapper.semanticReverseComparer())

        fun wrap(version: MinecraftVersions) = comparable.find { it.value == version } ?: error("Unknown Minecraft version $version")

        @JvmName("wrapExtension")
        fun MinecraftVersions.wrap() = wrap(this)

        fun requiredJavaVersion(minecraftVersion: SemanticVersion) = when {
            minecraftVersion <= MC1_16_5.semantic -> JavaSdkVersion.JDK_1_8
            minecraftVersion <= MC1_17_1.semantic -> JavaSdkVersion.JDK_16
            else -> JavaSdkVersion.JDK_17
        }

        fun fromSemantic(version: SemanticVersion) = values().firstOrNull { it.semantic == version }
    }
}
