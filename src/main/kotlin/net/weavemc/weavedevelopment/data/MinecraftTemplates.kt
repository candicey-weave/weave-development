package net.weavemc.weavedevelopment.data

import com.intellij.ide.fileTemplates.FileTemplateDescriptor
import com.intellij.ide.fileTemplates.FileTemplateGroupDescriptor
import com.intellij.ide.fileTemplates.FileTemplateGroupDescriptorFactory
import net.weavemc.weavedevelopment.asset.Assets

class MinecraftTemplates : FileTemplateGroupDescriptorFactory {
    override fun getFileTemplatesDescriptor(): FileTemplateGroupDescriptor {
        val group = FileTemplateGroupDescriptor("Minecraft", Assets.MINECRAFT_ICON)

        fun FileTemplateGroupDescriptor.addTemplates(vararg templates: String) = templates.forEach(::addTemplate)

        FileTemplateGroupDescriptor("Common", Assets.MINECRAFT_ICON).let { commonGroup ->
            group.addTemplate(commonGroup)
            commonGroup.addTemplate(FileTemplateDescriptor(GRADLE_GITIGNORE_TEMPLATE))
            commonGroup.addTemplate(FileTemplateDescriptor(MAVEN_GITIGNORE_TEMPLATE))
            commonGroup.addTemplate(FileTemplateDescriptor(GRADLE_WRAPPER_PROPERTIES))
        }

        FileTemplateGroupDescriptor("Weave", Assets.MINECRAFT_ICON).let { weaveGroup ->
            group.addTemplate(weaveGroup)
            weaveGroup.addTemplates(
                WEAVE_GRADLE_PROPERTIES_TEMPLATE,
                WEAVE_BUILD_GRADLE_TEMPLATE_GRVY,
                WEAVE_SETTINGS_GRADLE_TEMPLATE_GRVY,
                WEAVE_BUILD_GRADLE_TEMPLATE_KTS,
                WEAVE_SETTINGS_GRADLE_TEMPLATE_KTS,
                WEAVE_MIXINS_JSON_TEMPLATE,
                WEAVE_MOD_JSON_TEMPLATE,
            )
        }

        return group
    }

    companion object {
        const val WEAVE_BUILD_GRADLE_TEMPLATE_GRVY = "weave_build.gradle"
        const val WEAVE_SETTINGS_GRADLE_TEMPLATE_GRVY = "weave_settings.gradle"

        const val WEAVE_BUILD_GRADLE_TEMPLATE_KTS = "weave_build.gradle.kts"
        const val WEAVE_SETTINGS_GRADLE_TEMPLATE_KTS = "weave_settings.gradle.kts"

        const val WEAVE_GRADLE_PROPERTIES_TEMPLATE = "weave_gradle.properties"

        const val GRADLE_GITIGNORE_TEMPLATE = "Gradle.gitignore"
        const val MAVEN_GITIGNORE_TEMPLATE = "Maven.gitignore"

        const val GRADLE_WRAPPER_PROPERTIES = "WeaveDevelopment_gradle-wrapper.properties"
        const val WEAVE_MOD_JSON_TEMPLATE = "weave_weave.mod.json"

        const val WEAVE_MAIN_CLASS_JAVA_TEMPLATE = "weave_main_class.java"
        const val WEAVE_MAIN_CLASS_KOTLIN_TEMPLATE = "weave_main_class.kt"

        const val WEAVE_MIXINS_JSON_TEMPLATE = "weave_weave.mixins.json"
    }
}