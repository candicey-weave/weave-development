package net.weavemc.weavedevelopment.data

import net.weavemc.weavedevelopment.util.ComparableEnumWrapper

enum class MappingsTypes {
    SEARGE,
    MOJMAP,
    YARN;

    override fun toString(): String {
        return name[0] + name.substring(1).lowercase()
    }

    companion object {
        val comparable by ComparableEnumWrapper(values()) { current, other -> current.value.compareTo(other.value) }
    }
}