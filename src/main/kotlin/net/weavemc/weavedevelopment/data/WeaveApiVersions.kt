package net.weavemc.weavedevelopment.data

import net.weavemc.weavedevelopment.util.ComparableEnumWrapper
import net.weavemc.weavedevelopment.util.SemanticVersion

enum class WeaveApiVersions (
    versionString: String? = null,
    delimiter: String = ".",
) {
    NONE,
    V0_1_0;

    val semantic by lazy {
        if (this == NONE) {
            SemanticVersion.release(Int.MAX_VALUE, Int.MAX_VALUE, Int.MAX_VALUE)
        } else {
            val version = versionString?.split(delimiter) ?: name.substring(1).split("_")
            SemanticVersion.release(*version.map(String::toInt).toIntArray())
        }
    }

    override fun toString(): String = if (this == NONE) "None" else name.substring(1).replace('_', '.')

    companion object {
        val comparable by ComparableEnumWrapper.withSemantic(WeaveApiVersions.values(), WeaveApiVersions::semantic)

        fun wrap(version: WeaveApiVersions) = WeaveApiVersions.comparable.find { it.value == version } ?: error("Unknown Weave API version: $version")

        @JvmName("wrapExtension")
        fun WeaveApiVersions.wrap() = wrap(this)
    }
}