package net.weavemc.weavedevelopment.util

import com.intellij.lang.java.lexer.JavaLexer
import com.intellij.openapi.application.AppUIExecutor
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.application.ModalityState
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.util.Computable
import com.intellij.openapi.util.Condition
import com.intellij.openapi.util.Ref
import com.intellij.openapi.util.text.StringUtil
import com.intellij.pom.java.LanguageLevel
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiFile
import java.lang.invoke.MethodHandles
import java.util.*

// Bit of a hack, but this allows us to get the class object for top level declarations without having to
// put the whole class name in as a string (easier to refactor, etc.)
@Suppress("NOTHING_TO_INLINE") // In order for this to work this function must be `inline`
inline fun loggerForTopLevel() = Logger.getInstance(MethodHandles.lookup().lookupClass())

inline fun String.span(predicate: (Char) -> Boolean): Pair<String, String> {
    val prefix = takeWhile(predicate)
    return prefix to drop(prefix.length)
}

inline fun <T, R> Iterable<T>.mapFirstNotNull(transform: (T) -> R?): R? {
    forEach { element -> transform(element)?.let { return it } }
    return null
}

inline fun <T, R> Array<T>.mapFirstNotNull(transform: (T) -> R?): R? {
    forEach { element -> transform(element)?.let { return it } }
    return null
}

inline fun <T : Any> Iterable<T?>.forEachNotNull(func: (T) -> Unit) {
    forEach { it?.let(func) }
}

inline fun <T, reified R> Array<T>.mapToArray(transform: (T) -> R) = Array(size) { i -> transform(this[i]) }
inline fun <T, reified R> List<T>.mapToArray(transform: (T) -> R) = Array(size) { i -> transform(this[i]) }

fun Collection<*>.toArray(): Array<Any?> {
    @Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")
    return (this as java.util.Collection<*>).toArray()
}

inline fun <T : Any?> runWriteTask(crossinline func: () -> T): T {
    return invokeAndWait {
        ApplicationManager.getApplication().runWriteAction(Computable { func() })
    }
}

inline fun <T : Any?> PsiFile.runWriteAction(crossinline func: () -> T) =
    applyWriteAction { func() }

inline fun <T : Any?> PsiFile.applyWriteAction(crossinline func: PsiFile.() -> T): T {
    val result = WriteCommandAction.writeCommandAction(this).withGlobalUndo().compute<T, Throwable> { func() }
    val documentManager = PsiDocumentManager.getInstance(project)
    val document = documentManager.getDocument(this) ?: return result
    documentManager.doPostponedOperationsAndUnblockDocument(document)
    return result
}

fun invokeLater(func: () -> Unit) {
    ApplicationManager.getApplication().invokeLater(func, ModalityState.defaultModalityState())
}

fun invokeLater(expired: Condition<*>, func: () -> Unit) {
    ApplicationManager.getApplication().invokeLater(func, ModalityState.defaultModalityState(), expired)
}

fun invokeLaterAny(func: () -> Unit) {
    ApplicationManager.getApplication().invokeLater(func, ModalityState.any())
}

fun <T> invokeEdt(block: () -> T): T {
    return AppUIExecutor.onUiThread().submit(block).get()
}

fun <T : Any?> invokeAndWait(func: () -> T): T {
    val ref = Ref<T>()
    ApplicationManager.getApplication().invokeAndWait({ ref.set(func()) }, ModalityState.defaultModalityState())
    return ref.get()
}

fun String.isJavaKeyword() = JavaLexer.isSoftKeyword(this, LanguageLevel.HIGHEST)

fun String.toJavaIdentifier(allowDollars: Boolean = true): String {
    if (this.isEmpty()) {
        return "_"
    }

    if (this.isJavaKeyword()) {
        return "_$this"
    }

    if (!this[0].isJavaIdentifierStart() && this[0].isJavaIdentifierPart()) {
        return "_$this".toJavaIdentifier(allowDollars)
    }

    return this.asSequence()
        .map {
            if (it.isJavaIdentifierPart() && (allowDollars || it != '$')) {
                it
            } else {
                "_"
            }
        }
        .joinToString("")
}

fun String.toJavaClassName() = StringUtil.capitalizeWords(this, true)
    .replace(" ", "").toJavaIdentifier(allowDollars = false)

fun String.toPackageName(): String {
    if (this.isEmpty()) {
        return "_"
    }

    val firstChar = this.first().let {
        if (it.isJavaIdentifierStart()) {
            "$it"
        } else {
            ""
        }
    }
    val packageName = firstChar + this.asSequence()
        .drop(1)
        .filter { it.isJavaIdentifierPart() || it == '.' }
        .joinToString("")

    return if (packageName.isEmpty()) {
        "_"
    } else {
        packageName.lowercase(Locale.ENGLISH)
    }
}