package net.weavemc.weavedevelopment.util

import com.intellij.ide.fileTemplates.FileTemplateManager
import com.intellij.ide.starters.local.GeneratorTemplateFile
import com.intellij.ide.wizard.AbstractNewProjectWizardStep
import com.intellij.ide.wizard.GitNewProjectWizardData
import com.intellij.ide.wizard.NewProjectWizardStep
import com.intellij.notification.Notification
import com.intellij.notification.NotificationType
import com.intellij.openapi.project.Project
import net.weavemc.weavedevelopment.creator.getOrCreateClassKey
import net.weavemc.weavedevelopment.creator.project.step.buildsystem.FixedAssetsNewProjectWizardStep
import net.weavemc.weavedevelopment.data.MinecraftTemplates

inline fun <reified T : NewProjectWizardStep> NewProjectWizardStep.findStep(): T {
    return findStep(T::class.java)
}

fun <T : NewProjectWizardStep> NewProjectWizardStep.findStep(clazz: Class<out T>): T {
    return data.getUserData(getOrCreateClassKey(clazz))
        ?: throw IllegalStateException("Could not find required step ${clazz.name}")
}

val NewProjectWizardStep.gitEnabled
    get() = data.getUserData(GitNewProjectWizardData.KEY)!!.git

fun FixedAssetsNewProjectWizardStep.addGradleGitignore(project: Project) {
    addTemplates(project, ".gitignore" to MinecraftTemplates.GRADLE_GITIGNORE_TEMPLATE)
}

fun FixedAssetsNewProjectWizardStep.addMavenGitignore(project: Project) {
    addTemplates(project, ".gitignore" to MinecraftTemplates.MAVEN_GITIGNORE_TEMPLATE)
}

fun FixedAssetsNewProjectWizardStep.addTemplates(project: Project, vararg templates: Pair<String, String>) {
    addTemplates(project, templates.toMap())
}

fun FixedAssetsNewProjectWizardStep.addTemplates(project: Project, templates: Map<String, String>) {
    val manager = FileTemplateManager.getInstance(project)
    addAssets(templates.map { (path, template) -> GeneratorTemplateFile(path, manager.getJ2eeTemplate(template)) })
}

class EmptyStep(parent: NewProjectWizardStep) : AbstractNewProjectWizardStep(parent)

fun notifyCreatedProjectNotOpened() {
    Notification(
        "Weave project creator",
        "Created project must be opened",
        "Generated files might be incomplete and the project might be broken.",
        NotificationType.ERROR,
    ).notify(null)
}