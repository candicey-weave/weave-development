package net.weavemc.weavedevelopment.util

import com.intellij.openapi.vfs.LocalFileSystem
import com.intellij.openapi.vfs.VfsUtilCore
import com.intellij.openapi.vfs.VirtualFile
import java.io.File
import java.io.IOException
import java.nio.file.Path
import java.util.jar.Attributes
import java.util.jar.JarFile
import java.util.jar.Manifest

val VirtualFile.localFile: File
    get() = VfsUtilCore.virtualToIoFile(this)

val Path.virtualFile: VirtualFile?
    get() = LocalFileSystem.getInstance().refreshAndFindFileByPath(this.toAbsolutePath().toString())

val Path.virtualFileOrError: VirtualFile
    get() = virtualFile ?: throw IllegalStateException("Failed to find file: $this")

val VirtualFile.manifest: Manifest?
    get() = try {
        JarFile(localFile).use { it.manifest }
    } catch (e: IOException) {
        null
    }

operator fun Manifest.get(attribute: String): String? = mainAttributes.getValue(attribute)
operator fun Manifest.get(attribute: Attributes.Name): String? = mainAttributes.getValue(attribute)

fun VirtualFile.refreshFs(): VirtualFile {
    return this.parent.findOrCreateChildData(this, this.name)
}