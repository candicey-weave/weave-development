package net.weavemc.weavedevelopment.util

open class ComparableEnumWrapper<E>(
    private val values: Array<E>,
    val comparer: (current: ComparableEnumWrapper<E>.ValueWrapper, other: ComparableEnumWrapper<E>.ValueWrapper) -> Int
) where E : Enum<*> {
    open operator fun getValue(thisRef: Any?, property: Any?): List<ValueWrapper> = values.map(::ValueWrapper).sorted()

    open inner class ValueWrapper(val value: E) : Comparable<ValueWrapper> {
        override fun compareTo(other: ValueWrapper): Int = comparer(this, other)

        override fun equals(other: Any?): Boolean =
            other is ComparableEnumWrapper<*>.ValueWrapper && value == other.value

        override fun hashCode(): Int {
            var result = values.contentHashCode()
            result = 31 * result + value.hashCode()
            return result
        }

        override fun toString(): String = value.toString()
    }

    companion object {
        fun <F : Enum<*>> withSemantic(
            values: Array<F>,
            semanticProvider: (F) -> SemanticVersion,
            comparer: ((
                semanticProvider: (F) -> SemanticVersion,
                current: ComparableEnumWrapper<F>.ValueWrapper,
                other: ComparableEnumWrapper<F>.ValueWrapper
            ) -> Int)? = null
        ) =
            ComparableEnumWrapper(values) { current: ComparableEnumWrapper<F>.ValueWrapper, other: ComparableEnumWrapper<F>.ValueWrapper ->
                if (comparer == null) {
                    semanticProvider(current.value).compareTo(semanticProvider(other.value))
                } else {
                    comparer(semanticProvider, current, other)
                }
            }

        fun <G : Enum<*>> semanticReverseComparer() =
            { semanticProvider: (G) -> SemanticVersion, current: ComparableEnumWrapper<G>.ValueWrapper, other: ComparableEnumWrapper<G>.ValueWrapper ->
                -semanticProvider(current.value).compareTo(semanticProvider(other.value))
            }
    }
}
