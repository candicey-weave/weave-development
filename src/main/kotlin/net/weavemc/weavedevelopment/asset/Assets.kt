package net.weavemc.weavedevelopment.asset

import com.intellij.openapi.util.IconLoader
import javax.swing.Icon

object Assets {
    val MINECRAFT_ICON = loadIcon("/assets/icon/Minecraft.png")
    val MINECRAFT_ICON_2X = loadIcon("/assets/icon/Minecraft@2x.png")
}

private fun Assets.loadIcon(path: String): Icon = IconLoader.getIcon(path, Assets::class.java)