package net.weavemc.weavedevelopment.bundle

import com.intellij.DynamicBundle
import org.jetbrains.annotations.PropertyKey

private const val BUNDLE = "message.WeaveBundle"

object WeaveBundle : DynamicBundle(BUNDLE) {
    operator fun invoke(@PropertyKey(resourceBundle = BUNDLE) key: String): String {
        return getMessage(key)
    }

    operator fun invoke(@PropertyKey(resourceBundle = BUNDLE) key: String, vararg params: Any?): String {
        return getMessage(key, *params)
    }
}